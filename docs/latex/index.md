---
title: "使用VSCode编辑LaTeX"
slug: "LaTeX"
date: 2022-05-03T10:55:02+08:00
draft: false
---

如果您对在 LaTeX 编辑有兴趣，更详细的教程可以参考：[快速开始 - LaTeX](https://latex.all2doc.com/start/)

## 关于LaTeX

>LaTeX（/ˈlɑːtɛx/，常被读作/ˈlɑːtɛk/或/ˈleɪtɛk/，写作“LaTeX”），是一种基于TEX的排版系统，由美国计算机科学家莱斯利·兰伯特在20世纪80年代初期开发，利用这种格式系统的处理，即使用户没有排版和程序设计的知识也可以充分发挥由TEX所提供的强大功能，不必一一亲自去设计或校对，能在几天，甚至几小时内生成很多具有书籍质量的印刷品。对于生成复杂表格和数学公式，这一点表现得尤为突出。因此它非常适用于生成高印刷质量的科技和数学、物理文档。这个系统同样适用于生成从简单的信件到完整书籍的所有其他种类的文档。——[LaTeX - 维基百科，自由的百科全书](https://zh.wikipedia.org/zh-cn/LaTeX)

## 安装LaTeX

TeX Live 是 TUG (TeX User Group) 维护和发布的 TeX 系统，可说是「官方」的 TeX 系统。我们推荐任何阶段的 TeX 用户，都尽可能使用 TeX Live，以保持在跨操作系统平台、跨用户的一致性。TeX Live 的官方站点是 [TeX Live - TeX Users Group](https://tug.org/texlive/)。

Windows下，您可以直接使用[Windows - TeX Live - TeX Users Group](https://tug.org/texlive/windows.html)官方网站的安装工具，但TeXLive安装包大小共有4G，下载可能较慢。如果出现此情况，可以转到[Index of /CTAN/systems/texlive/Images/ | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/)下载。

![](2022-05-03-11-07-00.png)

下载完成后，点击下载好的文件即可打开，再点击install-tl-windows.bat文件进行安装即可。

安装选项中，可以将“安装TeXworks前端”取消，其他保持默认即可。安装过程大约20分钟。

检查安装是否正常： 按win + R 打开运行，输入cmd，打开命令行窗口；然后输入命令xelatex -v 。若显示找不到命令，您可能需要重启计算机。

## VSCode配置LaTeX

安装 LaTeX Workshop 和 LaTeX language support 两个插件。

![](2022-05-03-12-58-21.png)

转到`setting.json`（参考：[配置文件 - VSCode入门指南](https://vscode.all2doc.com/config/))，在最外层花括号内添加以下代码：

```json
    "LaTeX-workshop.LaTeX.autoBuild.run": "never",
    "LaTeX-workshop.showContextMenu": true,
    "LaTeX-workshop.intellisense.package.enabled": true,
    "LaTeX-workshop.message.error.show": false,
    "LaTeX-workshop.message.warning.show": false,
    "LaTeX-workshop.LaTeX.tools": [
        {
            "name": "xeLaTeX",
            "command": "xeLaTeX",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOCFILE%"
            ]
        },
        {
            "name": "pdfLaTeX",
            "command": "pdfLaTeX",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOCFILE%"
            ]
        },
        {
            "name": "LaTeXmk",
            "command": "LaTeXmk",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-pdf",
                "-outdir=%OUTDIR%",
                "%DOCFILE%"
            ]
        },
        {
            "name": "bibtex",
            "command": "bibtex",
            "args": [
                "%DOCFILE%"
            ]
        }
    ],
    "LaTeX-workshop.LaTeX.recipes": [
        {
            "name": "XeLaTeX",
            "tools": [
                "xeLaTeX"
            ]
        },
        {
            "name": "PDFLaTeX",
            "tools": [
                "pdfLaTeX"
            ]
        },
        {
            "name": "BibTeX",
            "tools": [
                "bibtex"
            ]
        },
        {
            "name": "LaTeXmk",
            "tools": [
                "LaTeXmk"
            ]
        },
        {
            "name": "xeLaTeX -> bibtex -> xeLaTeX*2",
            "tools": [
                "xeLaTeX",
                "bibtex",
                "xeLaTeX",
                "xeLaTeX"
            ]
        },
        {
            "name": "pdfLaTeX -> bibtex -> pdfLaTeX*2",
            "tools": [
                "pdfLaTeX",
                "bibtex",
                "pdfLaTeX",
                "pdfLaTeX"
            ]
        },
    ],
    "LaTeX-workshop.LaTeX.clean.fileTypes": [
        "*.aux",
        "*.bbl",
        "*.blg",
        "*.idx",
        "*.ind",
        "*.lof",
        "*.lot",
        "*.out",
        "*.toc",
        "*.acn",
        "*.acr",
        "*.alg",
        "*.glg",
        "*.glo",
        "*.gls",
        "*.ist",
        "*.fls",
        "*.log",
        "*.fdb_LaTeXmk"
    ],
    "LaTeX-workshop.LaTeX.autoClean.run": "onFailed",
    "LaTeX-workshop.LaTeX.recipe.default": "lastUsed",
    "LaTeX-workshop.view.pdf.internal.synctex.keybinding": "double-click"
```

关于此代码的详细解释以及自定义配置请参考

### Hello World


在**纯英文目录下**，新建`HelloWorld.tex`文件，写入以下内容：

```LaTeX
\documentclass{article}
\begin{document}
HelloWorld
\end{document}
```

打开此文件后，侧边导航栏显示TEX工作区：

![](2022-05-03-16-54-52.png)

点击`Recipe:XeLaTeX`编译，验证安装和配置情况。如无问题将会显示一个啥都没有的HelloWorld界面。

## 问题解决

### 是否正确安装

一般而言，完成 texlive 安装后，会自动修改 PATH 文件以便随时调用。

不过也存在 cmd 可以调用 XeLaTeX，VSCode 无法调用的情况。此时请重启电脑再次尝试。

### 编译器选择

当我们的文件不含引用时，请使用`XeLaTeX`或者`PDFLaTeX`，如果包含引用，请使用最后两个编译链。

### Debug

点击`View Log message`-`View LaTeX compiler`（第一个是插件的log，第二个才是编译器的log），即可查看错误信息。

一旦获得了错误信息，您就可以使用Google或者必应进行错误排查了。

## 自动补全与错误显示

VSCode 中编写 LaTeX 一大好处是可以使用其自动补全与错误提示。安装 LaTeX Workshop 之后，这些功能就自动配置好了。

使用`\`开头，以触发关键词补全，例如，要输入

```LaTeX
\begin{document}
\end{document}
```

只需输入`\doc...`。当然，也可以`\be...`然后选择合适的自动补全。

## 添加注释

和全局配置一致。`Ctrl+/`

## 附录

### Reference

1. [客_texlive安装需要多久](https://blog.csdn.net/qq_34769162/article/details/119752831)
2. [Visual Studio Code (vscode)配置LaTeX](https://zhuanlan.zhihu.com/p/166523064)
3. [LaTeX公式手册(全网最全) - 樱花赞 - 博客园](https://www.cnblogs.com/1024th/p/11623258.html)
4. [LaTeX中的各种文件及编译流程（附windows环境的完整编译脚本）_huitailangyz的博客-CSDN博客_LaTeX编译](https://blog.csdn.net/huitailangyz/article/details/99685683)


<!-- ### 版权声明

本文原载于https://zhelper.net -->
