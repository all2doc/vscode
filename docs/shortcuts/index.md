---
title: "VSCode 快捷键"
slug: "Shortcuts"
date: 2022-05-04T19:51:37+08:00
draft: false
---

## 快捷键

就其软件本体而言，VSCode并不能在效率上带来多大的提升，要用好VSCode，一定要了解他的快捷键、Snippet和插件。

官方快捷键参考：[keyboard-shortcuts-windows.pdf (visualstudio.com)](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf)

您可以通过快捷键`Ctrl+K Ctrl+R`快速打开此网站。

### 编辑器-单词/变量选择与替换

这是一个非常实用的功能。在编辑时我们时常会遇到一个变量/单词重复出现、需要批量修改的情况。VSCode对此支持很好。

首先，单击你需要修改的变量/单词，此时单词上会出现一个半透明的选中框，然后：

1. 按一次是选中当前单词，多次按下将按顺序选中下一处：`Ctrl+d`
2. 选择所有出现的当前单词：`Ctrl + Shift + L`或`Ctrl + F2` 
3. 重命名变量：`F2`

选中单词/变量之后，可以直接复制、删除或做替换。

### 编辑器-多光标与行操作

1. 多光标输入：`Alt`+点选
2. 按单词选中：`Ctrl + Shift + 右箭头/左箭头`
3. 在选定的每行末尾插入光标：`Shift + ALT + I`
4. 选择当前行：`Ctrl+L`

### 编辑器-格式化与跳转

1. 切换自动换行，当我们希望看到完整的行而无需水平滚动时，它是一个方便的小帮手。另外它也可以格式化由于换行导致的浏览上的不便：`ALT + Z`
2. 格式选择的代码：`Ctrl + K Ctrl + F`
3. 格式化文档（全部代码）：`Shift + ALT + F`
4. 删除尾部空格：`Ctrl + K Ctrl + X`


### 显示相关

1. 全屏：`F11`
2. 放大与缩小：`Ctrl +/-`
3. 显示和隐藏侧边栏：`Ctrl+B`（编辑时可能会有冲突）
4. 显示资源管理器： `Ctrl+Shift+E`
5. 显示搜索： `Ctrl+Shift+F`
6. 显示/打开终端：``Ctrl+Shift+` ``
7. 显示/隐藏问题（可以按两次快速关闭下边栏）：`ctrl-shift-m`
8. 显示 Git：`Ctrl+Shift+G`
9. 显示 Debug：`Ctrl+Shift+D`
10. 显示 Output（输出）： `Ctrl+Shift+U`
11. 进入Zen模式（简化所有内容，只留编辑器界面）：`Ctrl + K Z`

### 跳转

1. 转到上次编辑的位置：`Alt + ←`
2. 切换工作区（切换打开的文件夹）：`Ctrl+R`
3. 切换选项卡：`Ctrl + TAB`
4. 在资源管理器中打开当前文件：`Ctrl + K R`
5. 转到行（指定行数）：`Ctrl + G`
6. 转到定义`F12`


### 主命令框/设置/文件搜索

1. 打开主命令面板：`F1` 或 `Ctrl+Shift+P`
2. 打开文件搜索：`Ctrl+P`
>主命令框按一下 Backspace 会进入到 Ctrl+P 模式  
在 Ctrl+P 下输入 > 可以进入 Ctrl+Shift+P 模式
3. 打开用户设置：`Ctrl+,`
4. `Ctrl+K` `Ctrl+S`：显示/自定义快捷键

### 运行与调试

1. F8：转到下一个错误或警告


## 附录

### Reference

<!-- ### 版权声明

本文原载于https://zhelper.net -->
