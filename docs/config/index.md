---
title: "配置文件"
slug: "Config"
date: 2022-05-03T11:33:45+08:00
draft: false
---


VSCode使用`setting.json`存储所有全局配置。在每个打开的文件夹（工作区）下的`.vscode`文件，存储工作区配置。


### 配置全局设置

按`Ctrl+Shift+P`调出命令菜单，输入`setting`，点击 打开设置（json），即可调出设置文件。

如果您之前没有自定义过设置项，那么`setting.json`应该只有一个花括号：

```json
{
}
```

这代表所有选项均保持默认。

当您进行自定义设置时，花括号中每一句（以 , 结尾）均代表一个自定义配置，例如：

```json
{
    "git.autofetch": true,
    "git.confirmSync": false,
    "git.enableSmartCommit": true,
    "remote.SSH.remotePlatform": {
        "TEST": "linux",
    },
    // liveServer
    "liveServer.settings.donotVerifyTags": true,
    "jupyter.askForKernelRestart": false,
    "editor.foldingImportsByDefault": true,
    "editor.foldingStrategy": "indentation",
    // 关于markdown文件的配置
    "[markdown]": {
        "editor.formatOnSave": true,
        "editor.renderWhitespace": "all",
        "editor.quickSuggestions": {
            "other": true,
            "comments": true,
            "strings": true
        },
        "editor.acceptSuggestionOnEnter": "on"
    },
    "window.zoomLevel": -1
}
```

您可以从配置名中了解其含义。

配置文件也可以通过按下`Ctrl+,`（打开用户设置GUI界面），通过图形界面更改。不过对于资深用户更推荐直接使用`json`文件，更加方便。

如果您要手动添加配置，请一定注意`json`文件的格式。
- 所有配置项均需放置在最外层`{}`之间。
- 除了最后一行外，每一行结尾都必须加上`,`表示分隔。
- 如果一条配置还有多个子配置（如markdown），请使用`{}`括起来，内部也遵循第二条规则。
- 可以通过`//`添加注释。



### 配置同步

VSCode支持使用微软账号或GitHub账号进行同步。

## 附录

### Reference

### 版权声明

本文原载于https://latex.all2doc.com/
