---
title: "用VScode写markdown"
slug: "Vscode Markdown"
description: ""
date: 2022-01-12T10:10:51+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["笔记"]
---

## 缘起

其实我之前一直都是用Typora写markdown，虽然单就写作体验上说，Typora的所见即所得很不错，但是由于我写文章主要发布到博客上，每次用`hugo new`生成文章，然后Typora编辑，最后再Github Desktop上传，总还是有些不方便。

加上这一次放假回家，家里电脑也没装Typora，就说尝试一下用vscode写。

## 优势

用VScode写，相比Typora的一大好处就是集成程度更高，以往我需要三个软件进行的创建-编辑-上传过程，，现在只需要一个软件了。

将文件夹定位到博客根目录下，我可以直接在终端运行`hugo new`生成文章，然后使用众多的markdown插件辅助编辑，同时，在电脑上配置好Git的情况下，上传也非常方便。

>不过Git配置仍然还是比较痛苦的（国内网络太离谱了）
>如果你要用VScode配Git写文章，最好先把网络问题解决

关于Git仓库。Vscode中内置Git管理功能，当你使用`git clone`，拉取代码，再将Vscode定位到此文件夹中打开，即会自动激活Git功能。当然，需要首先安装git。

> 关于Git相关的更多内容，可以参考

## 劣势

就我目前使用体验而言，相较于Typora，VScode在两个方面显得有些孱弱。

**第一个是链接的插入**，Typora可以直接插入链接，自动获取目标网页标题，生成一段链接语法，而VScode不支持直接插入链接，需要先自己写网页标题，将标题文本选中之后，再`Ctrl+V`黏贴链接，生成丽娜姐语法。

**另外一个是图床功能**。目前我没有在VScode上找到合适的图床插件。但是的确有一键插入图片的插件，可以直接从剪贴板`Ctrl+Alt+V`贴图，图片会被自动保存在md文件所在目录下。以前我觉得这样不太好用，无法做到云同步。但其实，如果你把博客上传到GitHub上面的话，这些图片也会一起上传，只需要两台电脑之间多pull、push一下即可。然后对于hugo，其实他构建的时候也是支持本地引用的，这样公布开发布的问题也解决了，而且Netlify也会自动把你的图片CDN，岂不妙哉。最重要的一点，如果你用私人仓库，可以做到完全私人的云笔记（私有“图床”+私有文本）。

>对于Hugo使用本地图片，请参照本博客相关文章进行设置。不能直接放在文件夹下，无法识别

关于所见即所得功能，我想这还是见仁见智，我个人感觉有和没有没太大差别。当然如果你想用这个功能，也可以尝试使用Milkdown这个插件。但是这个插件还在开发，很不好用。

## 演示

![安装插件之后图片可以直接插入](2022-01-12-10-20-13.png)

## 开始使用

说了这么多，最重要的部分，还是如何配置起这样一个编辑环境。实际上，三个插件足矣。

### 插件

最好全装。

#### 必备插件

1. Markdown All in One：提供常用快捷键，列表生成，链接插入等基础功能

#### 可选插件

1. Markdownlint：语法检查，帮助撰写更加规范的文本，强迫症可以装。
2. Markdown Preview Enhanced：提供更好的预览，以及一些小功能。（参考：[Markdown Preview Enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/#/zh-cn/)）
3. Paste Image：直接从剪贴板插入图片（保存到当前目录下）。`Ctrl+Alt+V`
4. Paste URL：直接从剪贴板生成链接，自动获取标题。`Ctrl + Alt + P`
5. Auto-Open Markdown Preview：当打开 Markdown 文件时自动开启预览。


### 语法

1. 加粗-`Ctrl+B`
2. 斜体-`Ctrl+I`
3. 插入图片-`Ctrl+Alt+V`
4. 插入链接-`Ctrl+V`（先选中链接要展示的文本）
5. 预览-`Ctrl+K V`

## 使用snippet

由于vscode中markdown默认没有激活snippet功能，需要手动配置。

### 开启snippet

按`Ctrl+Shift+P`调出命令菜单，输入`setting`，点击 打开设置（json），调出设置文件。

在打开的文件中附加以下内容即可。

```json
"[markdown]": {
    "editor.formatOnSave": true,
    "editor.renderWhitespace": "all",
    "editor.quickSuggestions": {
        "other": true,
        "comments": true,
        "strings": true
    },
    "editor.acceptSuggestionOnEnter": "on"
}
```

![image-20220502134011883](image-20220502134011883.png)

### 自定义snippet（快捷插入代码片段）

按`Ctrl+Shift+P`调出命令菜单，输入`snippet`，点击 配置用户代码片段，点击 新建全局代码片段（对所有文件夹生效）/新建XXX文件夹代码片段（配置文件放在当前文件夹下，对本文件夹生效）。

`snippes.json`示例：

```
{
	"名称（不可重复）": {
		"prefix": "唤起此snippets的关键词",
		"body": [
			"第一行 $1 这里有一个待输入的参量",
			"第二行  行尾有第二个待输入的参量 $1"
		],
		"description": "描述"
	},
}
```

## 公式

行内公式

```
$ $
```

整行公式（居中）

```
$$ $$
```

[VScode插件Markdown Preview Enhanced自定义字体style_白马金羁侠少年的博客-CSDN博客](https://blog.csdn.net/qq_43827595/article/details/104983125)


## 附录

### Reference

1. https://zhuanlan.zhihu.com/p/113115306 没什么用
2. https://zhuanlan.zhihu.com/p/265197528 插件总结
3. https://zhuanlan.zhihu.com/p/139140492 插件总结（大量参考）
4. https://zhuanlan.zhihu.com/p/56943330 从零开始搭建，小白可看
4. [ Vscode Markdown Snippet 配置_Mr，yu的博客-CSDN博客](https://blog.csdn.net/serryuer/article/details/89393760)
5. [Markdown Preview Enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/#/zh-cn/)

<!-- ### 版权信息

本文原载于kermsite.com，复制请保留原文出处。 -->
