---
title: "VSCode 开始使用"
slug: "Start"
date: 2022-05-02T17:04:04+08:00
draft: false
---

## 软件下载

直接在官网进行下载

[Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)
[在中国快速下载Visual Studio Code &middot; 零壹軒·笔记](https://note.qidong.name/2020/05/dl-vscode-cn/)

对于大多数用户，直接点击首页中间的 Download for Windows 即可。

安装时，下面这两个可选项建议勾上，之后可以在windows的右键菜单里直接点击“Open with Code”，此操作会自动打开Vscode，而当前目录下所有文件都会出现在vscode的编辑区内。这样就可以用windows的资源管理器来管理你的文件，而在需要的时候随时用vscode进行编辑了。

![img](v2-5a8a9f0a9c41690ba97e40256456cb47_1440w.jpg)

## 快速开始

打开 VSCode，如下图：

![image-20220502180055918](image-20220502180055918.png)

### 切换中文

VSCode默认英文，打开，左侧导航栏最底下的图标就是vscode的插件市场，在这里可以找到中文包。安装中文包，重新打开 VSCode 即可。

![image-20220502171325000](image-20220502171325000.png)

VSCode 支持几乎所有语言的编辑。您可以现在就开始愉快地敲代码，也可以先了解 快捷键 和 配置文件

## 下一步

- [快捷键](https://vscode.all2doc.com/shortcuts/)
- [配置文件](https://vscode.all2doc.com/config/)
<!-- - 使用 VSCode 写 LaTeX
- 使用 VSCode 写 Markdown
- 使用 VSCode 写 Python
- 使用 VSCode 写 C/C++
- 使用 VSCode 写 HTML/JS/CSS -->

### 命令




## snippet



## 附录

### Reference

1. [keyboard-shortcuts-windows.pdf (visualstudio.com)](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf)
2. [23个常用的VSCode快捷键（动图演示）-VSCode-PHP中文网](https://www.php.cn/tool/vscode/441696.html)
3. [VS 和 VS Code回到上一次点击位置_centor的博客-CSDN博客_vs返回上一次操作位置](https://blog.csdn.net/centor/article/details/84862316)
4. [VS Code有哪些常用的快捷键？ - 知乎](https://www.zhihu.com/question/37623310/answer/149557815)



<!-- ### 版权声明

本文原载于https://zhelper.net -->
